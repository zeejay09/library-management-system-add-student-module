<?php include('admin-server.php'); 
	//If user is not logged in, they cannot access this page
if (empty($_SESSION['username'])) {
	header('location: login.php?secure-connection=1');
}
?>
<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<body>
<head>
	<title>Admin Dashboard</title>
</head>
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/3/w3.css"></link>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../css/main.css">
	<link rel="stylesheet" type="text/css" href="../css/welcome.css">

<div class="page-wrap">
	<div class="nav-wrapper">
		<ul class="topnav">
			<a href="main.php"><i class="fa fa-home"> Home</i></a>
			<a href="#books"><i class="fa fa-book"> Books</i></a>
			<a href="mng_stud.php"><i class="fa fa-user">  Students</i></a>
  		<div class="dropdown" style="float: right;">
              <button class="dropbtn" onclick="myFunction()"><i class="fa fa-user-circle-o" aria-hidden="true"><?php echo '&nbsp'.$_SESSION['username']; ?></i></button>
                  <div id="myDropdown" class="dropdown-content">
                  <a href="mng_pswd.php"><i class="fa fa-cog"> </i> Change password</a>
                  <a href="../welcome.php?logout='1'" ><i class="fa fa-sign-in"> Logout</i></a>
                  </div>
      </div>
		</ul>
	</div>

  <!-- Content will go here -->
  <div class="head-wrap">
    <h3>Welcome Librarian!</h3>
  </div>
  <div class="para-wrapper">
      <p class="title">Greetings!</p>
      <p class="paragraph">Hi librarian, this is your main screen. You have 2 buttons [book and students] on the navigation bar which is your main work - to manage books and students. You are in control of all the books issued to the library and to each students. You also have the authority to add, delete and update students in the Library Database. All functions will be explained below.</p>
      <p class="title">Librarian Functions</p>
      <p class="title">Books</p>
      <p class="paragraph">Click the book button on the navigation bar and it will take you to another interface which allows you to manage books like adding books that are newly issued to the library by entering its ISBN, full book title, its author and its quantity. You can also monitor how many particular books are left in the library after letting the students borrow. You can also update a book's information ie. when the school's book manufacturing company supplies the library with existing books, the quantity available of that particular book that is getting new supply will likely increase, so you must update its quantity available. You also monitor what books are in the library and lastly, you can delete particular books that are nearly obselete or the book manufacturer doesn't manufacture it anymore.</p>
      <p class="title">Students</p>
      <p class="paragraph">Click the students butoon on the navigation bar and it will redirect you to another page which allows you to manage students. You will be greeted with the manage students page which lets you add new students on the database and providing them with full details that is ask in the page. Also you have the authority to ban a student from borrwing from the library if they have not returned 3 books they borrowed consecutively. You also can update their information like if they shift to another course, you must update their college and course on where they shifted, it also goes the same their year level [1st, 2nd, 3rd, 4th, 5th, 5th++ year]. If students graduated from the school, you can either delete or keep their record/s. You can also track all students in all colleges, all year levels and courses.</p>
      <p class="title">Liability</p>
      <p class="paragraph">This function is embedded on every student, you can put a liability or add them to a liability list, on a student if they did not return the book after 3 days in possesion. If they returned the book after getting added to the liability list, you can easily delete them and you can also track who are in the liability list.</p>
      <p class="title" align="center">Attention! Change the default password if you haven't yet</p>
  </div>
  <div class="head-wrap">
    <h3>End of Page</h3>
  </div>

<!-- Footer -->
<footer class="w3-container w3-padding-64 w3-center w3-gray w3-xlarge">
  <a href="#"><i class="fa fa-facebook-official"></i></a>
  <a href="#"><i class="fa fa-pinterest-p"></i></a>
  <a href="#"><i class="fa fa-twitter"></i></a>
  <a href="#"><i class="fa fa-flickr"></i></a>
  <a href="#"><i class="fa fa-linkedin"></i></a>
  <p class="w3-medium">
    Brought to you by: <a href="#about?SLDevs+Profile='0'" target="_blank">SL Developers</a>
  </p>
</footer>
</div>


<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(e) {
  if (!e.target.matches('.dropbtn')) {
    var myDropdown = document.getElementById("myDropdown");
      if (myDropdown.classList.contains('show')) {
        myDropdown.classList.remove('show');
      }
  }
}
</script>

</body>
</html>