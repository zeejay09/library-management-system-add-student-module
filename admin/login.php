<?php include('admin-server.php'); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Admin Login</title>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<div class="header">
		<h2><i class="fa fa-lock" aria-hidden="true"> Admin</i></h2>
	</div>

	<form action="login.php" method="post">
	<!--Display validation errors here-->
		<?php include('errors.php'); ?>
		<div class="input-group">
			<label>Username</label>
			<input type="text" name="username" >
		</div>
		<div class="input-group">
			<label>Password</label>
			<input type="password" name="password" >
		</div>
		<div class="input-group">
			<button type="submit" name="login" class="btn">Login</button>
		</div>
	</form>
</body>
</html>