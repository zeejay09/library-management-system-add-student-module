<?php include('admin-server.php');
	//If user is not logged in, they cannot access this page
  if (empty($_SESSION['username'])) {
     header('location: login.php?secure-connection=1');
}
?>
<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<body>
<head>
	<title>Admin Dashboard</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!--TEMP CODES-->
	<link href='https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
	<link href='//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css' rel='stylesheet' type='text/css'>
	<link href='//cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/1.8/css/bootstrap-switch.css' rel='stylesheet' type='text/css'>
	<link href='https://davidstutz.github.io/bootstrap-multiselect/css/bootstrap-multiselect.css' rel='stylesheet' type='text/css'>
	<!--TEMP CODES-->
	
</head>
	<link rel="stylesheet" type="text/css" href="../css/show_stud.css">

	<div class="nav-wrapper">
		<ul class="topnav">
			<a href="main.php?back+home%true%"><i class="fa fa-home" aria-hidden="true"> Back</i></a>
			<a href="mng_stud.php"><i class="fa fa-plus-square-o" aria-hidden="true"> Add Student</i></a>
			<a href="del_stud.php"><i class="fa fa-trash-o" aria-hidden="true"> Delete Student</i></a>
			<a href="upd_stud.php"><i class="fa fa-refresh" aria-hidden="true"> Update Student</i></a>
		</ul>
	</div>
	
	<div class='container'>
	    <div class='panel panel-primary dialog-panel'>
	      <div class='panel-heading'>
			<h5>Student List</h5>
	      </div>
	      <div class='panel-body'>
	        <table>
			  <tr>
			    <th>IDNO</th>
			    <th>FIRSTNAME</th>
			    <th>LASTNAME</th>
			    <th>COURSE</th>
			    <th>YEAR</th>
			    <th>EMAIL</th>
			    <th>COLLEGE</th>
			    <th>STATUS</th>
			  </tr>
			  <tr>
			  	<?php include('show-stud-server.php'); ?>
			  </tr>
			</table>
	      </div>
	    </div>
	</div>
 </body>
 </html>