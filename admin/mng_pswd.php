<?php include('admin-server.php');
	//If user is not logged in, they cannot access this page
  if (empty($_SESSION['username'])) {
     header('location: login.php?secure-connection=1');
}
?>
<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<body>
<head>
	<title>Admin Dashboard</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!--TEMP CODES-->
	<link href='https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
	<link href='//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css' rel='stylesheet' type='text/css'>
	<link href='//cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/1.8/css/bootstrap-switch.css' rel='stylesheet' type='text/css'>
	<link href='https://davidstutz.github.io/bootstrap-multiselect/css/bootstrap-multiselect.css' rel='stylesheet' type='text/css'>
	<!--TEMP CODES-->

</head>
	<link rel="stylesheet" type="text/css" href="../css/main.css">

	<div class="nav-wrapper">
		<ul class="topnav">
			<a href="main.php"><i class="fa fa-home" aria-hidden="true"> Back</i></a>
		</ul>
	</div>
  
<link rel="stylesheet" type="text/css" href="../css/mng_stud.css">
<div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <h5>Change password form</h5>
      </div>
      <div class='panel-body'>
        <form class='form-horizontal' role='form' method="post" action="mng_pswd.php">
        <!--Display validations here-->
          <?php include('errors.php'); 
                include('confirms.php');
          ?>
          <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_number'>Old</label>
            <div class='col-md-2'>
              <input class='form-control' name="oldpass" placeholder='Old password' type='password'>
            </div>
          </div>
          <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_name'>New</label>
            <div class='col-md-8'>
              
              <div class='col-md-3 indent-small'>
                <div class='form-group internal'>
                  <input class='form-control' name="newpass" placeholder='New password' type='password'>
                </div>
              </div>
              <div class='col-md-3 indent-small'>
                <div class='form-group internal'>
                  <input class='form-control' name="confirmpass" placeholder='Confirm password' type='password'>
                </div>
              </div>
            </div>
          </div>

          <div class='form-group'>
            <div class='col-md-offset-4 col-md-3'>
              <button class='btn-lg btn-primary' name="updatepass" type='submit' onclick="return getConfirmation()">Update</button>
            </div>
            
          </div>
        </div>

        </form>
      </div>
    </div>
  </div>
<script type="text/javascript">
            function getConfirmation(){
               var retVal = confirm("Do you want to continue ?");
               if( retVal == true ) {
                  return true;
               }
               else{
                  return false;
               }
            }
</script>
</body>
</html>