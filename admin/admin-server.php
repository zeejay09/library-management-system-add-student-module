<?php include('class_lib.php'); ?>
<?php 
    session_start(); 
    $errors = array();
    $confirms = array();
    //Connect to PostgreSQL Server;
    $conn = new postgres;
    $conn->db_conn();

    //Log user in from login page
    if (isset($_POST['login'])) {
        $username = pg_escape_string($_POST['username']);
        $password = pg_escape_string($_POST['password']);
        
        $admin = new admin_auth($username, $password);

        if (empty($admin->get_user()) && empty($admin->get_pass())) {
            array_push($errors, "All fields are empty");
        } else {
            if (empty($admin->get_user())) {
            array_push($errors, "Username is required");
        }
            if (empty($admin->get_pass())) {
                array_push($errors, "Password is required");
            }
        }

        //Check if username and password has a match
        if (count($errors) == 0) {
            $query = "SELECT * FROM librarian WHERE username='$username' AND password='$password'";
            $result = pg_query($query);
            if (pg_num_rows($result) == 1 ) {
                //Log user in
                $_SESSION['username'] = $username;
                header('location: main.php'); //redirect to homepage
            } else {
                array_push($errors, "The username or password is incorrect");
            }
        }

    }

    //Add students from Manage Students
    if (isset($_POST['add'])) {
        $idno = pg_escape_string($_POST['idno']);
        $firstname = pg_escape_string($_POST['firstname']);
        $lastname = pg_escape_string($_POST['lastname']);
        $course = pg_escape_string($_POST['course']);
        $year = pg_escape_string($_POST['year']);
        $username = pg_escape_string($_POST['username']);
        $email = pg_escape_string($_POST['email']);
        $password = pg_escape_string($_POST['password']);
        $college = pg_escape_string($_POST['college']);

        //Ensures all required data are filled up
        if (empty($idno) || empty($firstname) || empty($lastname) || empty($course) || empty($year) || empty($username) || empty($email) || empty($password) || empty($college)) {
            array_push($errors, "Please fill up all fields");
        }

        if (count($errors) == 0) {
            $conn->insert_data($idno, $firstname, $lastname, $course, $year, $username, $email, $password, $college);
            array_push($confirms, "Successfully added");
        }
    }

    //User logout
    if (isset($_GET['logout'])) {
        session_destroy();
        unset($_SESSION['username']);
        header('location: welcome.php');
    }

    //Delete students
    if (isset($_POST['delete'])) {
        $idno = pg_escape_string($_POST['idno']);

        //Ensures that fields are filled up
        if (empty($idno)) {
            array_push($errors, "Please provide the idno");
        }

        if (count($errors) == 0) {
            $conn->delete_data($idno);
            array_push($confirms, "Successfully deleted");
        }
    }

    //Update students
    if (isset($_POST['update'])) {
        $idno = pg_escape_string($_POST['idno']);
        $college = pg_escape_string($_POST['college']);
        $course = pg_escape_string($_POST['course']);
        $year = pg_escape_string($_POST['year']);
        $status = pg_escape_string($_POST['status']);

        if (empty($idno)) {
            array_push($errors, "Please provide the ID No.");
        } else {
            if (empty($course) && empty($year) && empty($status)) {
                $conn->update_data("college", $college, $idno);
                array_push($confirms, "Successfully updated");
            } else if (empty($year) && empty($status)) {
                $conn->update_2d("college", "course", $college, $course, $idno);
                array_push($confirms, "Successfully updated");
            } else if (empty($status)) {
                $conn->update_3d("college", "course", "year", $college, $course, $year, $idno);
                array_push($confirms, "Successfully updated");
            } else if (empty($college) && empty($year) && empty($status)) {
                $conn->update_data("course", $course, $idno);
                array_push($confirms, "Successfully updated");
            } else if (empty($college) && empty($status)) {
                $conn->update_2d("course", "year", $course, $year, $idno);
                array_push($confirms, "Successfully updated");
            } else if (empty($college)) {
                $conn->update_3d("course", "year", "status", $course, $year, $status, $idno);
                array_push($confirms, "Successfully updated");
            } else if (empty($college) && empty($course) && empty($status)) {
                $conn->update_data("year", $year, $idno);
                array_push($confirms, "Successfully updated");
            } else if (empty($college) && empty($course)) {
                $conn->update_2d("year", "status", $year, $status, $idno);
                array_push($confirms, "Successfully updated");
            } else if (empty($course)) {
                $conn->update_3d("year", "status", "college", $year, $status, $college, $idno);
                array_push($confirms, "Successfully updated");
            } else if (empty($course) && empty($status)) {
                $conn->update_2d("college", "year", $college, $year, $idno);
                array_push($confirms, "Successfully updated");
            } else if (empty($course) && empty ($year)) {
                $conn->update_2d("college", "status", $college, $status, $idno);
                array_push($confirms, "Successfully updated");
            } else if (empty($college) && empty($year)) {
                $conn->update_2d("course", "status", $course, $status, $idno);
                array_push($confirms, "Successfully updated");
            } else {
                $conn->update_alld($idno, $college, $course, $year, $status);
                array_push($confirms, "Successfully updated");
            }
        }
    }

    //Admin update password
    if (isset($_POST['updatepass'])) {
        $oldpass = pg_escape_string($_POST['oldpass']);
        $newpass = pg_escape_string($_POST['newpass']);
        $confirmpass = pg_escape_string($_POST['confirmpass']);

        //Ensures all required data are filled up
        if (empty($oldpass) && empty($newpass) && empty($confirmpass)) {
            array_push($errors, "Please fill up all fields");
        } else {
            if (empty($oldpass)) {
                array_push($errors, "Please provide the old password");
            }
            if (empty($newpass) && empty($confirmpass)) {
                array_push($errors, "Please provide the new password");
            }
        }

        if ($newpass != $confirmpass) {
            array_push($errors, "Password do not match");
        }

        if (count($errors) == 0) {
            $stmt = "SELECT password FROM librarian WHERE password = '$oldpass'";
            $result = pg_query($stmt);
            if (pg_num_rows($result) == 1) {
                $conn->update_libdata("password", $newpass, $oldpass);
                array_push($confirms, "Successfully updated");
            } else {
                array_push($errors, "The old password is incorrect");
            }
        }
    }
?>