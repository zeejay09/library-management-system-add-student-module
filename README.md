# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is for the library management system that I created for our IT 151 Project
* Work in progress

### Contribution guidelines ###

* If you want to contribute, please always include a changelog and if you want to build another feature, please create another branch then
* push it afterwards, i'll be the one to merge it.
* Also please write code in a clean manner.

### Who do I talk to? ###

* JeexPoy (zeejaybelamide@gmail.com)